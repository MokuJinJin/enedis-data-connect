<?php

namespace Aeneria\EnedisDataConnectApi\Service;

/**
 * Meta-Service to access all API services
 *
 * @see https://datahub-enedis.fr/services-api/data-connect/documentation/
 */
interface DataConnectServiceInterface
{
    /**
     * Service d'autorisation et d'authentification
     *
     * @return AuthorizeServiceInterface
     */
    public function getAuthorizeService(): AuthorizeServiceInterface;

    /**
     * Service de récupération des mesures
     *
     * @return MeteringDataServiceInterface
     */
    public function getMeteringDataService(): MeteringDataServiceInterface;

    /**
     * Service d'information des clients
     *
     * @return CustomersServiceInterface
     */
    public function getCustomersService(): CustomersServiceInterface;
}
