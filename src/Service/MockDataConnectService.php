<?php

namespace Aeneria\EnedisDataConnectApi\Service;

class MockDataConnectService implements DataConnectServiceInterface
{
    /** @var AuthorizeServiceInterface */
    private $authorizeService;
    /** @var MeteringDataServiceInterface */
    private $meteringDataService;
    /** @var CustomersServiceInterface */
    private $customersService;

    public function __construct()
    {
        $this->authorizeService = new MockAuthorizeService();
        $this->meteringDataService = new MockMeteringDataService();
        $this->customersService = new MockCustomersService();
    }

    public function getAuthorizeService(): AuthorizeServiceInterface
    {
        return $this->authorizeService;
    }

    public function getMeteringDataService(): MeteringDataServiceInterface
    {
        return $this->meteringDataService;
    }

    public function getCustomersService(): CustomersServiceInterface
    {
        return $this->customersService;
    }
}
