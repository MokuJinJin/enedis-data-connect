<?php

namespace Aeneria\EnedisDataConnectApi\Service;

use Aeneria\EnedisDataConnectApi\Model\OAuthToken;
use Aeneria\EnedisDataConnectApi\Model\Token;
use Aeneria\EnedisDataConnectApi\Model\TokenInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Implements Authorize API
 *
 * @see https://datahub-enedis.fr/services-api/data-connect/documentation/autorisation-v1/
 */
class AuthorizeService extends AbstractApiService implements AuthorizeServiceInterface
{
    const GRANT_TYPE_CODE = 'authorization_code';
    const GRANT_TYPE_TOKEN = 'refresh_token';

    /** @var HttpClientInterface */
    private $httpClient;

    /** @var string */
    private $authEndpoint;
    /** @var string */
    private $tokenEndpoint;

    /** @var string */
    private $clientId;
    /** @var string */
    private $clientSecret;
    /** @var string */
    private $redirectUri;

    public function __construct(
        HttpClientInterface $httpClient,
        string $authEndpoint,
        string $tokenEndpoint,
        string $clientId,
        string $clientSecret,
        string $redirectUri
    ) {
        $this->httpClient = $httpClient;

        $this->authEndpoint = $authEndpoint;
        $this->tokenEndpoint = $tokenEndpoint;

        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUri = $redirectUri;
    }

    /**
     * {@inheritdoc}
     */
    public function getConsentPageUrl(string $duration, string $state): string
    {
        return \sprintf(
            '%s/dataconnect/v1/oauth2/authorize?client_id=%s&response_type=code&state=%s&duration=%s',
            $this->authEndpoint,
            $this->clientId,
            $state,
            $duration
        );
    }

    /**
     * @deprecated since 17/09/2023, use getOAuthToken() instead
     * {@inheritdoc}
     */
    public function requestTokenFromCode(string $code): Token
    {
        return $this->requestToken(self::GRANT_TYPE_CODE, $code);
    }

    /**
     * @deprecated since 17/09/2023, use getOAuthToken() instead
     * {@inheritdoc}
     */
    public function requestTokenFromRefreshToken(string $refreshToken): Token
    {
        return $this->requestToken(self::GRANT_TYPE_TOKEN, $refreshToken);
    }

    /**
     * core request for token
     * 
     * @deprecated since 17/09/2023, use getOAuthToken() instead
     *
     * @param string $grantType
     * @param string $codeOrToken
     * @return Token
     */
    private function requestToken(string $grantType, string $codeOrToken): Token
    {
        $query = [
            'redirect_uri' => $this->redirectUri,
        ];
        
        $body = [
            'grant_type' => $grantType,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
        ];

        switch ($grantType) {
            case self::GRANT_TYPE_CODE:
                $body['code'] = $codeOrToken;
                break;
            case self::GRANT_TYPE_TOKEN:
                $body['refresh_token'] = $codeOrToken;
                break;
            default:
                throw new \InvalidArgumentException(\sprintf(
                    'Only "%s" or "%s" grant types are supported',
                    self::GRANT_TYPE_TOKEN,
                    self::GRANT_TYPE_CODE
                ));
        }

        $response = $this->httpClient->request(
            'POST',
            //? \sprintf('%s/v1/oauth2/token', $this->tokenEndpoint),
            \sprintf('%s/oauth2/v3/token', $this->tokenEndpoint),
            [
                'query' => $query,
                'body' => $body,
            ]
        );

        $this->checkResponse($response);

        return Token::fromJson($response->getContent());
    }

    /**
     * get OAuthToken with clientID and clientSecret
     * 
     * @see https://datahub-enedis.fr/services-api/data-connect/documentation/jeton/
     *
     * @return OAuthToken
     */
    public function getOAuthToken(): OAuthToken
    {
        $body = ["grant_type" => "client_credentials"];
        $headers = ['Authorization' => 'Basic '. base64_encode($this->clientId.':'.$this->clientSecret)];

        $response = $this->httpClient->request(
            'POST',
            //? \sprintf('%s/v1/oauth2/token', $this->tokenEndpoint),
            \sprintf('%s/oauth2/v3/token', $this->tokenEndpoint),
            [
                'headers' => $headers,
                'body' => $body,
            ]
        );

        $this->checkResponse($response);

        return OAuthToken::fromJson($response->getContent());
    }
}
