<?php

namespace Aeneria\EnedisDataConnectApi\Service;

use Aeneria\EnedisDataConnectApi\Model\OAuthToken;
use Aeneria\EnedisDataConnectApi\Model\TokenInterface;

class MockAuthorizeService extends AbstractApiService implements AuthorizeServiceInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConsentPageUrl(string $duration, string $state): string
    {
        return 'http://enedis-dataconnect.fr/consent';
    }

    /**
     * {@inheritdoc}
     */
    public function requestTokenFromCode(string $code): TokenInterface
    {
        return TokenInterface::fromJson(<<<JSON
        {
            "access_token":"WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5",
            "scope":"am_application_scope default",
            "token_type":"Bearer",
            "expires_in":12600
        }
        JSON);
    }

    /**
     * {@inheritdoc}
     */
    public function requestTokenFromRefreshToken(string $refreshToken): TokenInterface
    {
        return TokenInterface::fromJson(<<<JSON
        {
            "access_token":"WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5",
            "scope":"am_application_scope default",
            "token_type":"Bearer",
            "expires_in":12600
        }
        JSON);
    }

    /**
     * {@inheritdoc}
     */
    public function getOAuthToken(): OAuthToken
    {
        return TokenInterface::fromJson(<<<JSON
        {
            "access_token":"WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5",
            "scope":"am_application_scope default",
            "token_type":"Bearer",
            "expires_in":12600
        }
        JSON);
    }
}
