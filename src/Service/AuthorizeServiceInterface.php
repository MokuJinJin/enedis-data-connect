<?php

namespace Aeneria\EnedisDataConnectApi\Service;

use Aeneria\EnedisDataConnectApi\Model\OAuthToken;
use Aeneria\EnedisDataConnectApi\Model\Token;
use Aeneria\EnedisDataConnectApi\Model\TokenInterface;

/**
 * Implements DataConnect AuthorizeV1 API
 *
 * @see https://datahub-enedis.fr/services-api/data-connect/documentation/autorisation-v1/
 */
interface AuthorizeServiceInterface
{
    /**
     * Get a URL to DataConnect consent page.
     *
     * @var string Durée du consentement demandé par l’application,
     * au format ISO 8601. Cette durée sera affichée au consommateur et ne peut
     * excéder 3 ans. (ex : P6M pour 6 mois)
     *
     * @var string Paramètre de sécurité permettant de maintenir l’état
     * entre la requête et la redirection.
     */
    public function getConsentPageUrl(string $duration, string $state): string;

    /**
     * Get DataConnectToken from a grant code.
     * @deprecated since 17/09/2023, use getOAuthToken() instead
     */
    public function requestTokenFromCode(string $code): TokenInterface;

    /**
     * Get DataConnectToken from a refreshToken.
     * @deprecated since 17/09/2023, use getOAuthToken() instead
     */
    public function requestTokenFromRefreshToken(string $refreshToken): TokenInterface;

    /**
     * get OAuthToken
     *
     * @return OAuthToken
     */
    public function getOAuthToken(): OAuthToken;
}
