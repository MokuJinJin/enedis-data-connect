<?php

namespace Aeneria\EnedisDataConnectApi\Service;

use Aeneria\EnedisDataConnectApi\Model\MeteringData;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Implements Metering Data V5 
 * 
 */
class MeteringDataService extends AbstractApiService implements MeteringDataServiceInterface
{
    /** @var HttpClientInterface */
    private $httpClient;
    /** @var string */
    private $dataEndpoint;

    public function __construct(HttpClientInterface $httpClient, string $dataEndpoint)
    {
        $this->httpClient = $httpClient;
        $this->dataEndpoint = $dataEndpoint;
    }

    /**
     * Consommation 30 minutes (load curve)
     * @see https://datahub-enedis.fr/services-api/data-connect/documentation/metering-v5-consommation-30-minutes/
     */
    public function requestConsumptionLoadCurve(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData
    {
        return $this->requestMeteringData(
            'metering_data_clc/v5/consumption_load_curve',
            MeteringData::TYPE_CONSUMPTION_LOAD_CURVE,
            $accessToken,
            $usagePointId,
            $start,
            $end
        );
    }

    /**
     * Production 30 minutes (load curve)
     * @see https://datahub-enedis.fr/services-api/data-connect/documentation/metering-v5-production-30-minutes/
     */
    public function requestProductionLoadCurve(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData
    {
        return $this->requestMeteringData(
            'metering_data_plc/v5/production_load_curve',
            MeteringData::TYPE_PRODUCTION_LOAD_CURVE,
            $accessToken,
            $usagePointId,
            $start,
            $end
        );
    }

    /**
     * Consommation Quotidienne
     * @see https://datahub-enedis.fr/services-api/data-connect/documentation/metering-v5-consommation-quotidienne/
     */
    public function requestDailyConsumption(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData
    {
        return $this->requestMeteringData(
            'metering_data_dc/v5/daily_consumption',
            MeteringData::TYPE_DAILY_CONSUMPTION,
            $accessToken,
            $usagePointId,
            $start,
            $end
        );
    }

    /**
     * Production Quotidienne
     * @see https://datahub-enedis.fr/services-api/data-connect/documentation/metering-v5-production-quotidienne/
     */
    public function requestDailyProduction(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData
    {
        return $this->requestMeteringData(
            'metering_data_dp/v5/daily_production',
            MeteringData::TYPE_DAILY_PRODUCTION,
            $accessToken,
            $usagePointId,
            $start,
            $end
        );
    }

    /**
     * Puissance maximale de consommation
     * @see https://datahub-enedis.fr/services-api/data-connect/documentation/metering-v5-puissance-maximum-de-consommation/
     * 
     * @return MeteringData
     */
    public function requestDailyMaxConsumption(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData
    {
        return $this->requestMeteringData(
            'metering_data_dcmp/v5/daily_consumption_max_power',
            MeteringData::TYPE_DAILY_MAX_CONSUMPTION,
            $accessToken,
            $usagePointId,
            $start,
            $end
        );
    }

    /**
     * Generic Request MeteringData.
     */
    private function requestMeteringData(string $endpoint, string $dataType, string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData
    {
        $response = $this->httpClient->request(
            'GET',
            // since v5 it is multiple API, different URI/endpoint
            \sprintf('%s/%s', $this->dataEndpoint, $endpoint),
            [
                'headers' => [
                    'accept' => 'application/json',
                    'Authorization' => 'Bearer ' .$accessToken
                ],
                'query' => [
                    'usage_point_id' => $usagePointId,
                    'start' => $start->format('Y-m-d'),
                    'end' => $end->format('Y-m-d'),
                ],
            ]
        );

        $this->checkResponse($response);
        return MeteringData::fromJson($response->getContent(), $dataType);
    }
}
