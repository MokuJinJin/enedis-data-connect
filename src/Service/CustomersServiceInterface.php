<?php

namespace Aeneria\EnedisDataConnectApi\Service;

use Aeneria\EnedisDataConnectApi\Model\Address;

/**
 * Implements DataConnect Customers API
 *
 */
interface CustomersServiceInterface
{
    /**
     * @see https://datahub-enedis.fr/services-api/data-connect/documentation/customers-v5-adresse/
     *
     * @param string $accessToken
     * @param string $usagePointId
     * @return Address
     */
    public function requestUsagePointAdresse(string $accessToken, string $usagePointId): Address;
}
