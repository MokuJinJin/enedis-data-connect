<?php

namespace Aeneria\EnedisDataConnectApi\Service;

use Aeneria\EnedisDataConnectApi\Model\MeteringData;

/**
 * Implements DataConnect Metering Data
 *
 */
interface MeteringDataServiceInterface
{
    /**
     * Get consumption load curve between 2 dates for a usage point.
     *
     * Récupérer la puissance moyenne consommée quotidiennement,
     * sur l'intervalle de mesure du compteur (par défaut 30 min)
     */
    public function requestConsumptionLoadCurve(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData;

    /**
     * Get production load curve between 2 dates for a usage point.
     *
     * Récupérer la puissance moyenne produite quotidiennement,
     * sur l'intervalle de mesure du compteur (par défaut 30 min)
     */
    public function requestProductionLoadCurve(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData;

    /**
     * Get daily consumption between 2 dates for a usage point.
     *
     * Récupérer la consommation quotidienne
     */
    public function requestDailyConsumption(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData;

    /**
     * Get daily production between 2 dates for a usage point.
     *
     * Récupérer la production quotidienne
     */
    public function requestDailyProduction(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData;

    /**
     * Cette API permet de récupérer la puissance maximale soutirée quotidienne pour le point de livraison de clients équipés de compteurs communicants Linky.
     * L’accès à cette ressource est conditionnée par le recueil préalable du consentement du client par Enedis.
     *
     * @return MeteringData
     */
    public function requestDailyMaxConsumption(string $accessToken, string $usagePointId, \DateTimeInterface $start, \DateTimeInterface $end): MeteringData;
}
