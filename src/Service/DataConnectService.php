<?php

namespace Aeneria\EnedisDataConnectApi\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Meta-Service to access all API services
 *
 * @see https://datahub-enedis.fr/services-api/data-connect/documentation/
 */
class DataConnectService implements DataConnectServiceInterface
{
    /** @var AuthorizeServiceInterface */
    private $authorizeService;
    /** @var MeteringDataServiceInterface */
    private $meteringDataService;
    /** @var CustomersService */
    private $customersService;

    public function __construct(HttpClientInterface $httpClient, string $authEndpoint, string $tokenEndpoint, string $dataEndpoint, string $clientId, string $clientSecret, string $redirectUri)
    {
        $this->authorizeService = new AuthorizeService($httpClient, $authEndpoint, $tokenEndpoint, $clientId, $clientSecret, $redirectUri);
        $this->meteringDataService = new MeteringDataService($httpClient, $dataEndpoint);
        $this->customersService = new CustomersService($httpClient, $dataEndpoint);
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthorizeService(): AuthorizeServiceInterface
    {
        return $this->authorizeService;
    }

    /**
     * {@inheritDoc}
     */
    public function getMeteringDataService(): MeteringDataServiceInterface
    {
        return $this->meteringDataService;
    }

    /**
     * {@inheritDoc}
     */
    public function getCustomersService(): CustomersServiceInterface
    {
        return $this->customersService;
    }
}
