<?php

namespace Aeneria\EnedisDataConnectApi\Model;

interface TokenInterface 
{
    /**
     * generate a token from JSON data from Enedis Auth
     *
     * @param string $jsonData
     * @return self
     */
    public static function fromJson(string $jsonData): self;

    /**
     * return auth token
     *
     * @return string
     */
    public function getAccessToken(): string;

    /**
     * check if accessTokenExpirationDate NOT older than today
     *
     * @return boolean
     */
    public function isAccessTokenStillValid(): bool;

    /**
     * return token's type
     *
     * @return string
     */ 
    public function getTokenType(): string;

    /**
     * return application's scope
     *
     * @return string
     */
    public function getScope(): string;
}