<?php

namespace Aeneria\EnedisDataConnectApi\Tests\Unit;

use Aeneria\EnedisDataConnectApi\Model\Token;
use Aeneria\EnedisDataConnectApi\Model\TokenInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

final class TokenTest extends TestCase
{
    public function testHydratation()
    {
        $data = <<<JSON
        {
            "access_token":"WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5",
            "scope":"am_application_scope default",
            "token_type":"Bearer",
            "expires_in":12600
        }
        JSON;

        $token = TokenInterface::fromJson($data);

        self::assertInstanceOf(TokenInterface::class, $token);
        self::assertSame("WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5", $token->getAccessToken());
        self::assertTrue($token->isAccessTokenStillValid());
        self::assertEquals("Bearer", $token->getTokenType());
        self::assertEquals("am_application_scope default", $token->getScope());
    }

    public function testSerialization()
    {
        $serializer = new Serializer(
            [
                new DateTimeNormalizer(),
                new ObjectNormalizer(null, null, null, new ReflectionExtractor()),
            ],
            [new JsonEncoder()]
        );

        $data = <<<JSON
        {
            "access_token":"WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5",
            "scope":"am_application_scope default",
            "token_type":"Bearer",
            "expires_in":12600
        }
        JSON;

        $token = TokenInterface::fromJson($data);
        $deserializedToken = $serializer->deserialize(
            $serializer->serialize($token, 'json'),
            TokenInterface::class,
            'json'
        );

        self::assertInstanceOf(TokenInterface::class, $deserializedToken);
        self::assertSame("WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5", $deserializedToken->getAccessToken());
        self::assertTrue($deserializedToken->isAccessTokenStillValid());
        self::assertEquals("Bearer", $deserializedToken->getTokenType());
        self::assertEquals("am_application_scope default", $deserializedToken->getScope());
    }
}
