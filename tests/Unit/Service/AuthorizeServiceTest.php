<?php

namespace Aeneria\EnedisDataConnectApi\Tests\Unit;

use Aeneria\EnedisDataConnectApi\Exception\DataConnectConsentException;
use Aeneria\EnedisDataConnectApi\Exception\DataConnectDataNotFoundException;
use Aeneria\EnedisDataConnectApi\Exception\DataConnectException;
use Aeneria\EnedisDataConnectApi\Exception\DataConnectQuotaExceededException;
use Aeneria\EnedisDataConnectApi\Model\Token;
use Aeneria\EnedisDataConnectApi\Model\TokenInterface;
use Aeneria\EnedisDataConnectApi\Service\AuthorizeService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

final class AuthorizeServiceTest extends TestCase
{
    public function testConsentPageUrl()
    {
        $service = new AuthorizeService(
            HttpClient::create(),
            'endpoint',
            "endpoint",
            'clientId',
            'clientSecrect',
            'redirectUri'
        );

        $consentUrl = $service->getConsentPageUrl('duration', 'state');

        self::assertSame('endpoint/dataconnect/v1/oauth2/authorize?client_id=clientId&response_type=code&state=state&duration=duration', $consentUrl);
    }

    public function testRequestTokenFromCode()
    {
        $json = <<<JSON
        {
            "access_token":"WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5",
            "scope":"am_application_scope default",
            "token_type":"Bearer",
            "expires_in":12600
        }
        JSON;
        $token = TokenInterface::fromJson($json);

        $httpClient = new MockHttpClient(
            new MockResponse($json)
        );

        $service = new AuthorizeService(
            $httpClient,
            'http://endpoint.fr',
            "https://ext.prod-sandbox.api.enedis.fr",
            'clientId',
            'clientSecrect',
            'redirectUri'
        );

        $tokenFromService = $service->getOAuthToken();

        self::assertEquals($token->getAccessToken(), $tokenFromService->getAccessToken());
        self::assertEquals($token->getTokenType(), $tokenFromService->getTokenType());
        self::assertEquals($token->getScope(), $tokenFromService->getScope());
    }

    public function testRequestTokenFromRefreshToken()
    {
        $json = <<<JSON
        {
            "access_token":"WeOAFUQA7KjyvWRujg6pqCNshq6pxJaC497Ubz3bku12lF4SW5Dws5",
            "scope":"am_application_scope default",
            "token_type":"Bearer",
            "expires_in":12600
        }
        JSON;
        $token = TokenInterface::fromJson($json);

        $httpClient = new MockHttpClient(
            new MockResponse($json)
        );

        $service = new AuthorizeService(
            $httpClient,
            'http://endpoint.fr',
            "https://ext.prod-sandbox.api.enedis.fr",
            'clientId',
            'clientSecrect',
            'redirectUri'
        );

        $tokenFromService = $service->getOAuthToken();

        self::assertEquals($token->getAccessToken(), $tokenFromService->getAccessToken());
        self::assertEquals($token->getTokenType(), $tokenFromService->getTokenType());
        self::assertEquals($token->getScope(), $tokenFromService->getScope());
    }

    public function test403Failure()
    {
        $httpClient = new MockHttpClient(
            new MockResponse('', ['http_code' => 403])
        );

        $service = new AuthorizeService(
            $httpClient,
            'http://endpoint.fr',
            "https://ext.prod-sandbox.api.enedis.fr",
            'clientId',
            'clientSecrect',
            'redirectUri'
        );

        $this->expectException(DataConnectConsentException::class);

        $service->getOAuthToken();
    }

    public function test404Failure()
    {
        $httpClient = new MockHttpClient(
            new MockResponse('', ['http_code' => 404])
        );

        $service = new AuthorizeService(
            $httpClient,
            'http://endpoint.fr',
            "https://ext.prod-sandbox.api.enedis.fr",
            'clientId',
            'clientSecrect',
            'redirectUri'
        );

        $this->expectException(DataConnectDataNotFoundException::class);

        $service->getOAuthToken();
    }

    public function test429Failure()
    {
        $httpClient = new MockHttpClient(
            new MockResponse('', ['http_code' => 429])
        );

        $service = new AuthorizeService(
            $httpClient,
            'http://endpoint.fr',
            "https://ext.prod-sandbox.api.enedis.fr",
            'clientId',
            'clientSecrect',
            'redirectUri'
        );

        $this->expectException(DataConnectQuotaExceededException::class);

        $service->getOAuthToken();
    }

    public function testOtherFailure()
    {
        $httpClient = new MockHttpClient(
            new MockResponse('', ['http_code' => 500])
        );

        $service = new AuthorizeService(
            $httpClient,
            'http://endpoint.fr',
            "https://ext.prod-sandbox.api.enedis.fr",
            'clientId',
            'clientSecrect',
            'redirectUri'
        );

        $this->expectException(DataConnectException::class);

        $service->getOAuthToken();
    }
}
