<?php

namespace Aeneria\EnedisDataConnectApi\Tests\Functional\Services;

use Aeneria\EnedisDataConnectApi\Model\Address;
use Aeneria\EnedisDataConnectApi\Model\MeteringData;
use Aeneria\EnedisDataConnectApi\Model\TokenInterface;
use Aeneria\EnedisDataConnectApi\Service\AuthorizeServiceInterface;
use Aeneria\EnedisDataConnectApi\Service\CustomersServiceInterface;
use Aeneria\EnedisDataConnectApi\Service\MeteringDataServiceInterface;
use Aeneria\EnedisDataConnectApi\Service\MockDataConnectService;
use PHPUnit\Framework\TestCase;

final class MockDataConnectServiceTest extends TestCase
{
    // @see https://datahub-enedis.fr/services-api/data-connect/ressources/decouvrir/
    private $usagePointID = "11453290002823";

    /**
     * That's a bit ugly but we test all DataConnectService methods in a single test
     * to avoid calling Enedis API to many times.
     */
    public function testMockDataConnectService()
    {
        $dataConnect = new MockDataConnectService();

        // Test Authorize V1 API
        $token = $this->gettingAccessToken($dataConnect->getAuthorizeService());
        
        // Test Metering Data V4 API
        $this->gettingConsumptionData($dataConnect->getMeteringDataService(), $token);
        // My client Id currently can't get production data, it's outside my authorized scope !
        $this->gettingProductionData($dataConnect->getMeteringDataService(), $token);

        // Test Customers API
        $this->gettingCustomerData($dataConnect->getCustomersService(), $token);
    }

    private function gettingAccessToken(AuthorizeServiceInterface $service): TokenInterface
    {
        $token = $service->getOAuthToken();

        self::assertInstanceOf(TokenInterface::class, $token);
        self::assertNotNull($token->getAccessToken());

        return $token;
    }

    private function gettingConsumptionData(MeteringDataServiceInterface $service, TokenInterface $token): void
    {
        $meteringData = $service->requestDailyConsumption(
            $token->getAccessToken(),
            $this->usagePointID,
            new \DateTimeImmutable('8 days ago midnight'),
            new \DateTimeImmutable('yesterday midnight')
        );

        self::assertInstanceOf(MeteringData::class, $meteringData);
        self::assertSame(MeteringData::TYPE_DAILY_CONSUMPTION, $meteringData->getDataType());
        self::assertSame(7, \count($meteringData->getValues()));
        self::assertSame('Wh', $meteringData->getUnit());

        $meteringValue = $meteringData->getValues()[0];
        self::assertEquals(new \DateInterval('P1D'), $meteringValue->getIntervalLength());
        self::assertNotNull($meteringValue->getValue());
        self::assertInstanceOf(\DateTimeInterface::class, $meteringValue->getDate());

        $meteringData = $service->requestConsumptionLoadCurve(
            $token->getAccessToken(),
            $this->usagePointID,
            new \DateTimeImmutable('2 days ago midnight'),
            new \DateTimeImmutable('yesterday midnight')
        );

        self::assertInstanceOf(MeteringData::class, $meteringData);
        self::assertSame(MeteringData::TYPE_CONSUMPTION_LOAD_CURVE, $meteringData->getDataType());
        self::assertGreaterThan(24, \count($meteringData->getValues()));
        self::assertSame('W', $meteringData->getUnit());

        $meteringValue = $meteringData->getValues()[0];
        self::assertEquals($meteringValue->getIntervalLength(), new \DateInterval('PT30M'));
        self::assertNotNull($meteringValue->getValue());
        self::assertInstanceOf(\DateTimeInterface::class, $meteringValue->getDate());
    }

    private function gettingProductionData(MeteringDataServiceInterface $service, TokenInterface $token): void
    {
        $meteringData = $service->requestDailyProduction(
            $token->getAccessToken(),
            $this->usagePointID,
            new \DateTimeImmutable('8 days ago midnight'),
            new \DateTimeImmutable('yesterday midnight')
        );

        self::assertInstanceOf(MeteringData::class, $meteringData);
        self::assertSame(MeteringData::TYPE_DAILY_PRODUCTION, $meteringData->getDataType());
        self::assertSame(7, \count($meteringData->getValues()));
        self::assertSame('Wh', $meteringData->getUnit());

        $meteringValue = $meteringData->getValues()[0];
        self::assertEquals(new \DateInterval('P1D'), $meteringValue->getIntervalLength());
        self::assertNotNull($meteringValue->getValue());
        self::assertInstanceOf(\DateTimeInterface::class, $meteringValue->getDate());

        $meteringData = $service->requestProductionLoadCurve(
            $token->getAccessToken(),
            $this->usagePointID,
            new \DateTimeImmutable('2 days ago midnight'),
            new \DateTimeImmutable('yesterday midnight')
        );

        self::assertInstanceOf(MeteringData::class, $meteringData);
        self::assertSame(MeteringData::TYPE_PRODUCTION_LOAD_CURVE, $meteringData->getDataType());
        self::assertGreaterThan(24, \count($meteringData->getValues()));
        self::assertSame('W', $meteringData->getUnit());

        $meteringValue = $meteringData->getValues()[0];
        self::assertEquals($meteringValue->getIntervalLength(), new \DateInterval('PT30M'));
        self::assertNotNull($meteringValue->getValue());
        self::assertInstanceOf(\DateTimeInterface::class, $meteringValue->getDate());
    }

    private function gettingCustomerData(CustomersServiceInterface $service, TokenInterface $token): void
    {
        $address = $service->requestUsagePointAdresse(
            $token->getAccessToken(),
            $this->usagePointID
        );

        self::assertInstanceOf(Address::class, $address);
        self::assertSame($this->usagePointID, $address->getUsagePointId());
    }
}
